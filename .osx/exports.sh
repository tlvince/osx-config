# Homebrew
export PATH="/usr/local/sbin:$PATH"

# Node (brewed)
export PATH="$PATH:/usr/local/share/npm/bin"

# Android SDK (brewed)
export ANDROID_HOME="/usr/local/opt/android-sdk"

# System
export PATH="$PATH:/opt/local/bin"

# Karma
export CHROME_BIN="/opt/homebrew-cask/Caskroom/chromium/latest/chrome-mac/Chromium.app/Contents/MacOS/Chromium"
export FIREFOX_BIN="/opt/homebrew-cask/Caskroom/firefox/latest/Firefox.app/Contents/MacOS/firefox"

# Local
export PATH="$HOME/bin:$PATH"
